﻿using QuickSave.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Custom_Farm_Loader.ModEntry;

namespace Custom_Farm_Loader.API
{
    internal class QuickSaveAPIHandler
    {
        private static IQuickSaveAPI Api;
        public static bool IsQuickSaveLoading => Api?.IsLoading ?? false;

        public static void Initialize()
        {
            _Helper.Events.GameLoop.GameLaunched += GameLaunched;
        }

        private static void GameLaunched(object sender, StardewModdingAPI.Events.GameLaunchedEventArgs e)
        {
            if (!_Helper.ModRegistry.IsLoaded("DLX.QuickSave"))
                return;

            Api = _Helper.ModRegistry.GetApi<IQuickSaveAPI>("DLX.QuickSave");
        }
    }
}
