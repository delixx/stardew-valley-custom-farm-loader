﻿using Custom_Farm_Loader.Menus;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json.Linq;
using StardewModdingAPI;
using StardewValley;
using StardewValley.GameData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Custom_Farm_Loader.Lib
{
    public class CustomFarm
    {
        private static bool LoadedCachedCustomFarms = false;
        public static List<CustomFarm> CachedCustomFarms = new List<CustomFarm>();
        public static List<ModFarmType> CachedModFarmTypes = new List<ModFarmType>();

        private static Mod Mod;
        private static IMonitor Monitor;
        private static IModHelper Helper;
        private static Texture2D MissingMapIcon;

        private static readonly List<string> VanillaTypes = new List<string> { "standard", "riverland", "forest", "hills", "wilderness", "four corners", "beach", "meadowlands" };
        private static CustomFarm CurrentCustomFarm = null;
        private static string CurrentCustomFarmId = "";

        private bool IsCFLMap = false;
        public bool IsVanillaMap = false;
        private ModFarmType ModFarmType; //Only used for non CFL maps
        public IManifest Manifest;

        public string UniqueModID = "";
        public string ID = "";

        public string Name = "Unknown Farm";
        public string Description = "<none>";
        public List<KeyValuePair<string, string>> TranslatedDescriptions = new List<KeyValuePair<string, string>>();

        public Texture2D Icon;
        public Texture2D WorldMapOverlay;
        public string Preview = "";
        private string IconValue = "";
        private string WorldMapOverlayValue = "";

        public string Author = "Unknown";
        public int MaxPlayers = 0;
        public string MapFile = "";

        public List<KeyValuePair<string, Point>> Warps = new List<KeyValuePair<string, Point>>();
        public List<KeyValuePair<string, Point>> Locations = new List<KeyValuePair<string, Point>>();

        public List<Furniture> StartFurniture = new();
        public List<StartBuilding> StartBuildings = new();
        public List<DailyUpdate> DailyUpdates = new();
        public List<FishingRule> FishingRules = new();
        public FarmProperties Properties = new();


        public string ContentPackDirectory = ""; // The mod directory of the map we're loading
        public string RelativeContentPackPath = ""; // The mod folder of the map we're loading as a relative path to CFL
        public string RelativeMapDirectoryPath = ""; // The map directory of the map we're loading relative its mod folder
        public CustomFarm() { }

        public CustomFarm(ModFarmType modFarmType)
        {
            ModFarmType = modFarmType;
        }

        public CustomFarm(bool isVanilla, string iconValue, string worldMapOverlayValue)
        {
            IsVanillaMap = isVanilla;
            IconValue = iconValue;
            WorldMapOverlayValue = worldMapOverlayValue;
        }

        public static void Initialize(Mod mod)
        {
            Mod = mod;
            Monitor = mod.Monitor;
            Helper = mod.Helper;

            MissingMapIcon = ModEntry._Helper.ModContent.Load<Texture2D>("assets/MissingMapIcon.png");

            Helper.Events.GameLoop.GameLaunched += GameLaunched;
            Helper.Events.Content.LocaleChanged += (s, e) => clearCache();
            Helper.Events.GameLoop.ReturnedToTitle += (s, e) => {
                if (Context.ScreenId == 0 && LocalizedContentManager.CurrentLanguageCode != LocalizedContentManager.LanguageCode.en)
                    clearCache();
            };
        }

        public static void clearCache() {
            CachedCustomFarms.Clear();
            CachedModFarmTypes.Clear();
            CurrentCustomFarmId = null;
            getAll();
            Helper.GameContent.InvalidateCache("Data/AdditionalFarms");
            getCurrentCustomFarm();
        }

        private static void GameLaunched(object sender, StardewModdingAPI.Events.GameLaunchedEventArgs e)
        {
            //Cache all maps on gamelaunch to decrease load later
            getAll();
        }

        public static CustomFarm parseMapJson(string mapJson, string contentPackDirectory, IManifest manifest)
        {
            JObject o;
            CustomFarm customFarm = new CustomFarm();
            customFarm.ContentPackDirectory = contentPackDirectory.Split(Path.DirectorySeparatorChar).Last();
            string relativeMapJsonPath = mapJson.Substring(contentPackDirectory.Count() + 1);
            customFarm.RelativeMapDirectoryPath = relativeMapJsonPath.Contains(Path.DirectorySeparatorChar)
                ? relativeMapJsonPath.Substring(0, (relativeMapJsonPath.Count() - relativeMapJsonPath.Split(Path.DirectorySeparatorChar).Last().Count() - 1))
                : "";
            customFarm.RelativeContentPackPath = Path.GetRelativePath(Helper.DirectoryPath + Path.DirectorySeparatorChar, contentPackDirectory + Path.DirectorySeparatorChar);

            customFarm.UniqueModID = manifest.UniqueID;
            customFarm.Author = manifest.Author;
            customFarm.Manifest = manifest;
            customFarm.IsCFLMap = true;

            try {
                o = JObject.Parse(File.ReadAllText(mapJson));
            } catch (Exception ex) {
                Monitor.Log($"Failed to parse cfl_map.json in path:\n{customFarm.ContentPackDirectory}{Path.DirectorySeparatorChar}{relativeMapJsonPath}", LogLevel.Error);
                Monitor.Log(ex.Message, LogLevel.Error);
                return null;
            }

            string name = "";
            try {
                JEnumerable<JToken> childNodes = o.Children();

                foreach (JProperty n in childNodes) {
                    if (n.Value.Type == JTokenType.Null)
                        continue;

                    name = n.Name;
                    string value = n.Value.ToString();

                    switch (name.ToLower()) {
                        case "id" or "farmtypeid":
                            customFarm.ID = value;
                            break;
                        case "name":
                            customFarm.Name = value;
                            break;
                        case "author":
                            customFarm.Author = value;
                            break;
                        case "description":
                            customFarm.Description = value;
                            break;
                        case "mapfile":
                            customFarm.MapFile = value;
                            break;
                        case "maxplayers":
                            customFarm.MaxPlayers = int.Parse(value);
                            break;
                        case "icon":
                            customFarm.IconValue = value;
                            break;
                        case "preview":
                            if (value != "")
                                customFarm.Preview = $"{customFarm.RelativeContentPackPath}{customFarm.RelativeMapDirectoryPath}{Path.DirectorySeparatorChar}{value}";
                            break;
                        case "worldmapoverlay":
                            customFarm.WorldMapOverlayValue = value;
                            customFarm.WorldMapOverlay = customFarm.loadWorldMapTexture();
                            break;
                        case "warps":
                            customFarm.Warps = parseWarps(n);
                            break;
                        case "locations":
                            customFarm.Locations = parseLocations(n);
                            break;
                        case "startfurniture":
                            customFarm.StartFurniture = Furniture.parseFurnitureJsonArray(n);
                            break;
                        case "startbuildings":
                            customFarm.StartBuildings = StartBuilding.parseJsonArray(n);
                            break;
                        case "dailyupdates":
                            customFarm.DailyUpdates = DailyUpdate.parseDailyUpdateJsonArray(n, manifest);
                            break;
                        case "fishingrules":
                            customFarm.FishingRules = FishingRule.parseFishingRuleJsonArray(n, manifest);
                            break;
                        case "properties":
                            customFarm.Properties = FarmProperties.parseJObject(n);
                            break;
                        case "descriptiontranslations":
                            customFarm.TranslatedDescriptions = parseTranslatedDescriptionJObject(n);
                            break;
                        default:
                            Monitor.Log("Unknown cfl_map.json Attribute", LogLevel.Error);
                            throw new ArgumentException($"Unknown cfl_map.json Attribute", name);
                    }
                }

                if (customFarm.ID is null || customFarm.ID == "")
                    customFarm.ID = $"{customFarm.UniqueModID}/{customFarm.Name}";

                return customFarm;
            } catch (Exception ex) {
                Monitor.Log($"Failed to parse cfl_map.json {(name != "" ? $"at '{name}' " : "")}in path:\n{customFarm.ContentPackDirectory}{Path.DirectorySeparatorChar}{relativeMapJsonPath}", LogLevel.Error);
                Monitor.Log(ex.Message, LogLevel.Error);
            }


            return null;
        }

        private static List<KeyValuePair<string, Point>> parseWarps(JProperty jArray)
        {
            List<KeyValuePair<string, Point>> ret = new List<KeyValuePair<string, Point>>();
            string name = "";
            string value = "";

            try {
                foreach (JProperty jProperty in jArray.First()) {
                    if (jProperty.Value.Type == JTokenType.Null)
                        continue;

                    name = jProperty.Name;
                    value = jProperty.Value.ToString();

                    if (value == "")
                        continue;

                    switch (name.ToLower().Replace(" ", "")) {
                        case "busstop" or "forest" or "backwoods" or "farmcave" or "warptotem":
                            ret.Add(new KeyValuePair<string, Point>(name, UtilityMisc.parsePoint(value)));
                            break;
                        default:
                            Monitor.Log("Unknown Warps Attribute", LogLevel.Error);
                            throw new ArgumentException($"Unknown Warps Attribute", name);
                    }
                }
            } catch (Exception ex) {
                Monitor.Log($"At Warps -> '{name}'", LogLevel.Error);
                Monitor.Log(ex.Message, LogLevel.Trace);
                throw;
            }

            return ret;
        }

        private static List<KeyValuePair<string, Point>> parseLocations(JProperty jArray)
        {
            List<KeyValuePair<string, Point>> ret = new List<KeyValuePair<string, Point>>();
            string name = "";
            string value = "";

            try {
                foreach (JProperty jProperty in jArray.First()) {
                    if (jProperty.Value.Type == JTokenType.Null)
                        continue;

                    name = jProperty.Name;
                    value = jProperty.Value.ToString();

                    if (value == "")
                        continue;

                    switch (name.ToLower().Replace(" ", "")) {
                        case "farmhouse" or "greenhouse" or "spousearea" or "petbowl" or "mailbox" or "shippingbin" or "grandpashrine":
                            ret.Add(new KeyValuePair<string, Point>(name, UtilityMisc.parsePoint(value)));
                            break;
                        default:
                            Monitor.Log("Unknown Locations Attribute", LogLevel.Error);
                            throw new ArgumentException($"Unknown Locations Attribute", name);
                    }
                }
            } catch (Exception ex) {
                Monitor.Log($"At Locations -> '{name}'", LogLevel.Error);
                Monitor.Log(ex.Message, LogLevel.Trace);
                throw;
            }

            return ret;
        }

        private static List<KeyValuePair<string, string>> parseTranslatedDescriptionJObject(JProperty jObject)
        {
            List<KeyValuePair<string, string>> ret = new List<KeyValuePair<string, string>>();

            foreach (JProperty jProperty in jObject.First()) {
                if (jProperty.Value.Type == JTokenType.Null)
                    continue;

                ret.Add(new KeyValuePair<string, string>(jProperty.Name, (string)jProperty.Value));
            }

            return ret;
        }

        public void reloadTextures()
        {
            if (!IsCFLMap)
                return;

            //We load the icon and world map textures late to give recolor mods time to patch them.
            //The world map is additionally being reloaded on daystart to allow for seasonal world maps
            if (Icon == null)
                Icon = Helper.GameContent.Load<Texture2D>(asModFarmType().IconTexture);

            Helper.GameContent.InvalidateCache(asset => asset.NameWithoutLocale.IsEquivalentTo(asModFarmType().WorldMapTexture));
            WorldMapOverlay = Helper.GameContent.Load<Texture2D>(asModFarmType().WorldMapTexture);
        }

        public Texture2D loadIconTexture()
        {
            string path = RelativeContentPackPath + RelativeMapDirectoryPath;

            if (IconValue == "") return MissingMapIcon;

            if (Game1.mouseCursors == null)
                Game1.mouseCursors = Game1.content.Load<Texture2D>("LooseSprites\\Cursors");

            switch (IconValue.ToLower()) {
                case "standard":
                    return UtilityMisc.createSubTexture(Game1.mouseCursors, new Rectangle(2, 324, 18, 20));
                case "riverland":
                    return UtilityMisc.createSubTexture(Game1.mouseCursors, new Rectangle(24, 324, 19, 20));
                case "forest":
                    return UtilityMisc.createSubTexture(Game1.mouseCursors, new Rectangle(46, 324, 18, 20));
                case "hills":
                    return UtilityMisc.createSubTexture(Game1.mouseCursors, new Rectangle(68, 324, 18, 20));
                case "wilderness":
                    return UtilityMisc.createSubTexture(Game1.mouseCursors, new Rectangle(90, 324, 18, 20));
                case "four corners":
                    return UtilityMisc.createSubTexture(Game1.mouseCursors, new Rectangle(2, 345, 18, 20));
                case "beach":
                    return UtilityMisc.createSubTexture(Game1.mouseCursors, new Rectangle(24, 345, 18, 20));
                case "meadowlands":
                    return UtilityMisc.createSubTexture(Helper.GameContent.Load<Texture2D>("LooseSprites\\farm_ranching_icon"), new Rectangle(2, 0, 18, 20));
            }

            try {
                var icon = Helper.ModContent.Load<Texture2D>($"{path}{Path.DirectorySeparatorChar}{IconValue}");
                return CustomFarmSelection.cropIcon(icon);

            } catch (Exception ex) {
                Monitor.LogOnce($"Unable to load the map icon in:\n{path}{Path.DirectorySeparatorChar}{IconValue}", LogLevel.Warn);
            }
            return MissingMapIcon;
        }

        public Texture2D loadWorldMapTexture()
        {
            string path = RelativeContentPackPath + RelativeMapDirectoryPath;
            string worldMapFile = WorldMapOverlayValue;

            if (worldMapFile == "") return null;

            if (VanillaTypes.Contains(worldMapFile.ToLower())) {
                var seasonSuffix = Game1.season switch {
                    Season.Summer => "_summer",
                    Season.Fall => "_fall",
                    Season.Winter => "_winter",
                    _ => ""
                };

                Helper.GameContent.InvalidateCache($"LooseSprites\\map{seasonSuffix}");
                Texture2D map = Helper.GameContent.Load<Texture2D>($"LooseSprites\\map{seasonSuffix}");
                switch (worldMapFile.ToLower()) {
                    case "standard":
                        return UtilityMisc.createSubTexture(map, new Rectangle(0, 43, 131, 61));
                    case "riverland":
                        return UtilityMisc.createSubTexture(map, new Rectangle(0, 180, 131, 61));
                    case "forest":
                        return UtilityMisc.createSubTexture(map, new Rectangle(131, 180, 131, 61));
                    case "hills":
                        return UtilityMisc.createSubTexture(map, new Rectangle(0, 241, 131, 61));
                    case "wilderness":
                        return UtilityMisc.createSubTexture(map, new Rectangle(131, 241, 131, 61));
                    case "four corners":
                        return UtilityMisc.createSubTexture(map, new Rectangle(0, 302, 131, 61));
                    case "beach":
                        return UtilityMisc.createSubTexture(map, new Rectangle(131, 302, 131, 61));
                    case "meadowlands":
                        return Helper.GameContent.Load<Texture2D>($"LooseSprites\\Farm_ranching_map{seasonSuffix}");
                }
            }

            try {
                return Helper.ModContent.Load<Texture2D>($"{path}{Path.DirectorySeparatorChar}{getSeasonSuffixedFile(worldMapFile)}");
            } catch (Exception ex) { }

            try {
                return Helper.ModContent.Load<Texture2D>($"{path}{Path.DirectorySeparatorChar}{worldMapFile}");

            } catch (Exception ex) {
                Monitor.LogOnce($"Unable to load the world map texture in:\n{path}{Path.DirectorySeparatorChar}{worldMapFile}", LogLevel.Warn);
            }

            WorldMapOverlayValue = "";
            return null;
        }

        public static string getSeasonSuffixedFile(string file)
        {
            var seasonSuffix = Game1.season switch {
                Season.Summer => "_summer",
                Season.Fall => "_fall",
                Season.Winter => "_winter",
                _ => "_spring"
            };

            string fName = Path.GetFileNameWithoutExtension(file);
            string fExt = Path.GetExtension(file);
            return Path.Combine(fName + seasonSuffix + fExt);
        }


        public static CustomFarm getCurrentCustomFarm()
        {
            if (Game1.whichModFarm is not null &&
                CurrentCustomFarmId != Game1.whichModFarm.Id) {
                CurrentCustomFarmId = Game1.whichModFarm.Id;
                CurrentCustomFarm = get(Game1.whichModFarm.Id);
            }

            return CurrentCustomFarm;
        }

        public ModFarmType asModFarmType()
        {
            if (!IsCFLMap)
                return ModFarmType;

            return new ModFarmType() {
                Id = this.ID,
                MapName = $"CFL_Map/{this.ID}",
                TooltipStringPath = $"Strings/UI:CFL_Description/{this.ID}",
                IconTexture = $"CFL_Icon/{this.ID}",
                WorldMapTexture = $"CFL_WorldMap/{this.ID}",
                ModData = new Dictionary<string, string>()
            };
        }

        public static CustomFarm get(string id, bool isCFLMap = true)
        {
            if (!LoadedCachedCustomFarms) getAll();

            IEnumerable<CustomFarm> maps = CachedCustomFarms.FindAll(el => el.ID == id && el.IsCFLMap == isCFLMap);

            if (maps.Count() > 1) {
                Monitor.Log($"(!) Multiple Custom Farms with the same ID found: '{id}'", LogLevel.Error);
                Monitor.Log($"Please make sure Custom Farm IDs are unique", LogLevel.Error);

                foreach (CustomFarm map in maps) {
                    Monitor.Log($"{map.ContentPackDirectory}{Path.DirectorySeparatorChar}{map.RelativeMapDirectoryPath}", LogLevel.Warn);
                }
            } else if (maps.Count() == 1)
                return maps.First();
            else
                Monitor.LogOnce($"Failed to load Custom Farm with the id '{id}'", LogLevel.Trace);
            return null;
        }

        public static List<CustomFarm> getAll()
        {
            if (CachedCustomFarms.Count > 0)
                return CachedCustomFarms;

            CachedCustomFarms = new List<CustomFarm>();
            CachedModFarmTypes = new List<ModFarmType>();

            foreach (var contentPack in Mod.Helper.ContentPacks.GetOwned())
                CachedCustomFarms.AddRange(getAllFromModFolder(contentPack.DirectoryPath, contentPack.Manifest));

            CachedCustomFarms = CachedCustomFarms.OrderBy(o => o.Name).ToList();
            LoadedCachedCustomFarms = true;

            return CachedCustomFarms;
        }

        public static List<ModFarmType> getAllAsModFarmType()
        {
            if (CachedModFarmTypes.Count() > 0)
                return CachedModFarmTypes;

            getAll().ForEach(el => CachedModFarmTypes.Add(el.asModFarmType()));

            return CachedModFarmTypes;
        }

        public static List<CustomFarm> getAllFromModFolder(string modFolder, IManifest manifest)
        {
            List<CustomFarm> ret = new List<CustomFarm>();
            IEnumerable<string> maps = Directory.EnumerateFiles(modFolder, "cfl_map.json", new EnumerationOptions() { RecurseSubdirectories = true }).AsParallel();

            foreach (string mapJson in maps) {
                CustomFarm customFarm = parseMapJson(mapJson, modFolder, manifest);
                if (customFarm != null)
                    ret.Add(customFarm);
            }

            return ret;
        }

        public static bool IsCFLMapSelected()
        {
            return Game1.whichFarm == 7
                && getCurrentCustomFarm() != null
                && getCurrentCustomFarm().IsCFLMap;
        }

        public string getLocalizedDescription()
        {
            if (IsVanillaMap)
                return Description;

            var description = Game1.content.LoadString(asModFarmType().TooltipStringPath);

            if (IsCFLMap)
                return description;
            else
                return description.Substring(description.IndexOf('_') + 1);

        }
    }
}
