﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Custom_Farm_Loader.Lib.Enums
{
    public enum BackgroundType
    {
        None,
        Any,
        Beach,
        Dirt,
        Grass,
        Stone,
        Wood
    }
}
