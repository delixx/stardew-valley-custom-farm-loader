﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Custom_Farm_Loader.Lib.Enums
{
    public enum DailyUpdateType
    {
        None,
        TransformWeeds,
        SpawnResourceClumps,
        SpawnQuarryRocks,
        SpawnBeachDrops,
        SpawnForagingDrops,
        SpawnForestFarmDrops,
        SpawnItemDrops,
        SpawnWildCrops
    }
}
