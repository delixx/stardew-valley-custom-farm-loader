﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Custom_Farm_Loader.Lib.Enums
{
    //I decided not to support custom crops, because I deemed it not worth the effort.
    //It'd make testing and implementing way more tedious and I wasn't sure anyone would even want to use it.
    //Especially since there's custom logic at place like for Spring_Onion and at least in 1.5 iirc different crops had different ways to create the objects for, like with flowers
    //Wild Crops are probably already yet another feature no one is going to use in the first place, so unless someone specifically asks for this, it's a big no
    public enum WildCropType
    {
        Spring_Onion = 399,
        Ginger = 829,

        Garlic = 476, //248
        Parsnip = 472, //24
        Potato = 475, //192
        Radish = 484, //264
        Wheat = 483, //262
        Bok_Choy = 491, //278
        Fiber = 885, //771
        Blue_Jazz = 429, //597
        Tulip = 427, //591
        Poppy = 453, //376
        Summer_Spangle = 455, //593
        Sunflower = 431, //421
        Amaranth = 299, //300
        Fairy_Rose = 425, //595

        //1.1.7
        Cauliflower = 474, //190
        Coffee = 433, //395
        Green_Bean = 473, //188
        Kale = 477, //250
        Rhubarb = 478, //252
        Strawberry = 745, //400
        Rice = 273, //423
        Blueberry = 481, //258
        Corn = 487, //270
        Hops = 302, //304

        Hot_Pepper = 482, //260
        Melon = 479, //254
        Red_Cabbage = 485, //266
        Starfruit = 486, //268
        Tomato = 480, //256
        Artichoke = 489, //274
        Beet = 494, //284
        Cranberry = 493, //282
        Eggplant = 488, //272
        Grape = 301, //398

        Pumpkin = 490, //608
        Yam = 492, //280
        Ancient_Fruit = 499, //454
        Cactus = 802, //90
        Mixed_Seeds = 770,
        Pineapple = 833, //832
        Taro = 831, //830
        Sweet_Gem_Berry = 347, //417
        //Tea = 251, //815 BUGGED, tea isn't a crop, it's a bush
    }
}
