﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Custom_Farm_Loader
{
    public class ModConfig
    {
        public float LoadMenuIconScale { get; set; }
        public float CoopMenuIconScale { get; set; }
        
        public bool IncludeVanilla { get; set; }
        public bool DisableStartFurniture { get; set; }

        public ModConfig()
        {
            LoadMenuIconScale = 1f;
            CoopMenuIconScale = 1f;
            IncludeVanilla = false;
            DisableStartFurniture = false;
        }

    }
}
